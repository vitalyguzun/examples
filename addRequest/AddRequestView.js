import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import Translate from 'react-translate-component';

import { addRequest } from 'rapp/actions/asyncActions';
import { withAsyncActions } from 'experium-modules';
import { validate } from 'experium-modules';
import { required, maxLength, maxValue, minValue } from 'rapp/utils/validators';
import ManagerForm from 'rapp/components/addRequest/ManagerForm';
import BasicInformationForm from 'rapp/components/addRequest/BasicInformationForm';
import WorkConditionsForm from 'rapp/components/addRequest/WorkConditionsForm';
import RequirementsForm from 'rapp/components/addRequest/RequirementsForm';

const stateToProps = ({ form: { addRequestForm }, meta, user }) => (
    { addRequestForm, meta: meta.addRequest, user }
)

const schema = {
    responsible:      required('errors.responsibleNotBlank'),
    organization:     required('errors.companyNotBlank'),
    position:         required('errors.positionNotBlank'),
    quantity:         [required('errors.enterQuantity'), minValue(1)],
    compensations:    maxLength(1023),
    probation:        maxLength(3),
    requirement:      maxLength(1023),
    responsibilities: maxLength(1023),
    education: {
        comment:      maxLength(1023)
    },
    personality:      maxLength(1023),
    age: {
        from:         [minValue(0), maxValue(200)],
        to:           [minValue(0), maxValue(200)]
    }
}

@withAsyncActions({ addRequest })
@connect(stateToProps)
@reduxForm({ form: 'addRequestForm', validate: validate(schema) })
export default class AddRequestView extends Component {
    onSubmit = (form) => {
        this.props.addRequest.dispatch({ ...form });
    }

    render() {
        const { handleSubmit } = this.props;

        return (
            <form className='form-horizontal card-form' onSubmit={handleSubmit(this.onSubmit)} noValidate>

                <ManagerForm {...this.props} />
                <BasicInformationForm {...this.props} />
                <WorkConditionsForm {...this.props} />
                <RequirementsForm {...this.props} />

                <div className='form-group m-t-20'>
                    <div className='col-sm-9 col-sm-offset-3'>
                        <button type='submit' className='btn btn-primary waves-effect'>
                            <Translate content='request.add' component='span' />
                        </button>
                    </div>
                </div>

            </form>
        );
    }
}
