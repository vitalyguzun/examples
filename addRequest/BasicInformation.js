import React, { Component } from 'react';
import Translate from 'react-translate-component';
import translate from 'counterpart';
import { Field } from 'redux-form';
import { path } from 'ramda';

import { withAsyncActions } from 'experium-modules';
import { getDepartment, getTowns, getReplacing, getOrganizations } from 'rapp/actions/asyncActions';
import SearchGroup from 'rapp/components/addRequest/SearchGroup';
import FormGroup from 'rapp/components/form/FormGroup';

const replacingRequired = (value) => !value ? translate('errors.required') : undefined;

@withAsyncActions({ getDepartment, getTowns, getReplacing, getOrganizations })
export default class BasicInformationForm extends Component {
    componentWillMount() {
        this.props.getOrganizations.dispatch();
    }

    componentDidMount() {
        const company = path(['user', 'company'], this.props);

        if (company) {
            this.props.initialize({ organization: company.toString() });
            this.props.getDepartment.dispatch({ company });
        }
    }

    onReasonChange = (e, value) => {
        if (value === 7006 && !this.props.replacing) {
            const organizationId = path(['addRequestForm' ,'values', 'organization'], this.props);
            const departmentId = path(['addRequestForm', 'values', 'department'], this.props);
            this.props.getReplacing.dispatch({ organizationId, departmentId });
        }
    }

    onOrganizationsChange = (event, company) => {
        this.props.getDepartment.dispatch({ company });
    }

    onSearch = (value) => {
        this.props.getTowns.dispatch({ value });
    }

    render () {
        const {
            getOrganizations: { data: organizations },
            getDepartment: { data: department },
            getTowns: { data: towns },
            getReplacing: { data: replacing },
            addRequestForm
        } = this.props;

        const reasonVal = path(['values', 'reason'], addRequestForm);

        if (department && !department.length) {
            department.unshift({ id: 0, level: 0, name: translate('common.headquarters') });
        }

        return (
            <div className="card">
                <div className="card-header">
                    <h2>
                        <i className="zmdi zmdi-info m-r-5"></i>&nbsp;
                        <Translate content='request.headers.basicInformation' component='span' />
                    </h2>
                </div>

                <Field name='organization' allowClear
                    onChange={this.onOrganizationsChange}
                    component={FormGroup} type='select'
                    list={organizations}
                    label={{ content: 'request.company', hint: 'form.necessarily' }}
                    placeholder={translate('placeholders.chooseCompany')} />

                <Field name='department' allowClear
                    component={FormGroup} type='select'
                    list={department}
                    label={{ content: 'request.department' }}
                    className='form-control'
                    placeholder={translate('placeholders.chooseDepartment')} />

                <Field name='position'
                    component={FormGroup}
                    label={{ content: 'request.position' , hint: 'form.necessarily' }}
                    placeholder={translate('placeholders.enterPosition')} />

                <Field name='town' allowClear showSearch
                    component={SearchGroup}
                    onSearch={this.onSearch}
                    list={towns}
                    label={{ content: 'request.town' }}
                    placeholder={translate('placeholders.enterTown')} />

                <Field name='quantity'
                    component={FormGroup} type='number'
                    label={{ content: 'request.quantity' , hint: 'form.necessarily' }}
                    placeholder={translate('placeholders.quantity')} />

                <Field name='deadline'
                    component={FormGroup} type='datepicker'
                    label={{ content: 'request.deadline' }}
                    placeholder={translate('placeholders.deadline')} />

                <Field name='budget'
                    component={FormGroup} type='radio'
                    loadName='budget'
                    namePath='value'
                    label={{ content: 'request.budget' }} />

                <Field name='reason'
                    onChange={this.onReasonChange}
                    component={FormGroup} type='radio'
                    loadName='reason'
                    namePath='value'
                    label={{ content: 'request.reason' }} />

                { reasonVal === 7006 &&
                    <Field name='replacing' allowClear
                        component={FormGroup} type='select'
                        list={replacing}
                        namePath='firstName'
                        label={{ content: 'request.replacing' }}
                        placeholder={translate('placeholders.chooseEmployee')}
                        validate={[ replacingRequired ]} />
                }
            </div>
        );
    }
}
