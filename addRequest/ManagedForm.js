import React, { Component } from 'react';
import Translate from 'react-translate-component';
import translate from 'counterpart';
import { Field } from 'redux-form';

import { withAsyncActions } from 'experium-modules';
import { getResponsibleUsers } from 'rapp/actions/asyncActions';
import FormGroup from 'rapp/components/form/FormGroup';

@withAsyncActions({ getResponsibleUsers })
export default class ManagerForm extends Component {
    componentWillMount() {
        this.props.getResponsibleUsers.dispatch();
    }

    render () {
        const { getResponsibleUsers: { data: responsible }} = this.props;

        return (
            <div className='card'>
                <div className='card-header'>
                    <h2>
                        <i className='zmdi zmdi-account m-r-5'></i>&nbsp;
                        <Translate content='request.headers.hr_manager' component='span' />
                    </h2>
                </div>

                <Field name='responsible' allowClear
                    component={FormGroup} type='select'
                    list={responsible}
                    namePath='firstName'
                    label={{ content: 'request.responsible', hint: 'form.necessarily' }}
                    placeholder={translate('placeholders.chooseManager')} />
            </div>
        );
    }
};
