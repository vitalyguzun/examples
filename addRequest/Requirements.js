import React, { Component } from 'react';
import Translate from 'react-translate-component';
import translate from 'counterpart';
import { Field } from 'redux-form';
import { path, filter } from 'ramda';

import { withAsyncActions } from 'experium-modules';
import { getLanguage, getLanguageLevel } from 'rapp/actions/asyncActions';
import FormGroup from 'rapp/components/form/FormGroup';

const lessThanAgeTo = (value, model) => {
    const to = path(['age', 'to'], model);
    return to && (value > to) ? translate('errors.mustBeLessThan', {value: to}) : undefined;
}

const moreThanAgeFrom = (value, model) => {
    const from = path(['age', 'from'], model);
    return from && (value < from) ? translate('errors.mustBeMoreThan', {value: from}) : undefined;
}

@withAsyncActions({ getLanguage, getLanguageLevel })
export default class RequirementsForm extends Component {
    componentWillMount() {
        this.props.getLanguage.dispatch();
        this.props.getLanguageLevel.dispatch();
    }

    render () {
        const { getLanguageLevel: { data: languageLevel }, addRequestForm} = this.props;

        let { getLanguage: { data: language }} = this.props;
        language = filter((lang) => !!lang.name, language);
        const languages = path(['values', 'languages'], addRequestForm);

        return (
            <div className="card">
                <div className="card-header">
                    <h2>
                        <i className="zmdi zmdi-graduation-cap m-r-5"></i>&nbsp;
                        <Translate content='request.headers.requirementsForCandidate' component='span' />
                    </h2>
                </div>

                <Field name='requirement'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.requirements' }}
                    placeholder={translate('placeholders.enterExperienceRequirements')} />

                <Field name='responsibilities'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.responsibilities' }}
                    placeholder={translate('placeholders.enterResponsibilities')} />

                <Field name='education.level' allowClear
                    component={FormGroup} type='select'
                    namePath='value'
                    loadName='education'
                    label={{ content: 'request.education' }}
                    placeholder={translate('placeholders.chooseEducation')} />

                <Field name='education.comment'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.educationComment' }}
                    placeholder={translate('placeholders.enterEducationComment')} />

                <Field name='personality'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.personalQualities' }}
                    placeholder={translate('placeholders.enterPersonalQualities')} />

                <div className='row card-body card-padding'>
                    <div className='col-sm-3 control-label'>
                        <Translate content='common.languages' component='span' />
                    </div>

                    <div className='col-sm-9 p-l-0 p-r-0'>
                        <Field name='languages[0].id' allowClear
                            component={FormGroup} type='select'
                            list={language}
                            containerClass='col-sm-6'
                            placeholder={translate('placeholders.chooseLanguage')} />
                        { languages && languages[0] &&
                            <Field name='languages[0].level.id' allowClear
                                component={FormGroup} type='select'
                                list={languageLevel}
                                namePath='value'
                                containerClass='col-sm-6'
                                placeholder={translate('placeholders.proficiency')} />
                        }

                        { languages && languages[0] && languages[0].level &&
                            <div>
                                <Field name='languages[1].id' allowClear
                                    component={FormGroup} type='select'
                                    list={language}
                                    containerClass='col-sm-6'
                                    placeholder={translate('placeholders.chooseLanguage')} />
                                <Field name='languages[1].level.id' allowClear
                                    component={FormGroup} type='select'
                                    list={languageLevel}
                                    namePath='value'
                                    containerClass='col-sm-6'
                                    placeholder={translate('placeholders.proficiency')} />
                            </div>
                        }

                        { languages && languages[1] && languages[1].level &&
                            <div>
                                <Field name='languages[2].id' allowClear
                                    component={FormGroup} type='select'
                                    list={language}
                                    containerClass='col-sm-6'
                                    placeholder={translate('placeholders.chooseLanguage')} />
                                <Field name='languages[2].level.id' allowClear
                                    component={FormGroup} type='select'
                                    list={languageLevel}
                                    namePath='value'
                                    containerClass='col-sm-6'
                                    placeholder={translate('placeholders.proficiency')} />
                            </div>
                        }

                    </div>
                </div>

                <Field name='sex'
                    component={FormGroup} type='radio'
                    loadName='sex'
                    namePath='value'
                    label={{ content: 'request.sex' }} />

                <div className='row card-body card-padding'>
                    <div className='col-sm-3 control-label'>
                        <Translate content='request.age.title' component='span' />
                    </div>
                    <div className='col-sm-9 p-l-0 p-r-0'>
                        <Field name='age.from'
                            component={FormGroup} type='number'
                            containerClass='col-sm-3'
                            placeholder={translate('placeholders.from')}
                            validate={[ lessThanAgeTo ]} />
                        <Field name='age.to'
                            component={FormGroup} type='number'
                            containerClass='col-sm-3'
                            placeholder={translate('placeholders.to')}
                            validate={[ moreThanAgeFrom ]} />
                    </div>
                </div>

                <Field name='experience' allowClear
                    component={FormGroup} type='select'
                    namePath='value'
                    loadName='experience'
                    label={{ content: 'request.experience' }}
                    placeholder={translate('placeholders.chooseExperience')} />

                <Field name='comment'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.comment' }}
                    placeholder={translate('placeholders.enterAdditionalInformation')} />

            </div>
        );
    }
};
