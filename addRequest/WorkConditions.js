import React, { Component } from 'react';
import Translate from 'react-translate-component';
import translate from 'counterpart';
import { Field } from 'redux-form';
import { path } from 'ramda';

import { SALARY_NUMBER } from 'rapp/constants/regexp';
import { withAsyncActions } from 'experium-modules';
import { getCurrency, getGrossnet } from 'rapp/actions/asyncActions';
import FormGroup from 'rapp/components/form/FormGroup';
import TaxationRadioButtons from 'rapp/components/addRequest/TaxationRadioButtons';

const salaryNumberScan = (value) => {
    return value && !SALARY_NUMBER.test(value) ? translate('errors.invalidSalaryValue') : undefined;
}

const lessThanSalaryTo = (value, model) => {
    const to = path(['salary', 'amount', 'to'], model);
    return to && (value > to) ? translate('errors.mustBeLessThan', {value: to}) : undefined;
}

const moreThanSalaryFrom = (value, model) => {
    const from = path(['salary', 'amount', 'from'], model);
    return from && (value < from) ? translate('errors.mustBeMoreThan', {value: from}) : undefined;
}

@withAsyncActions({ getCurrency, getGrossnet })
export default class WorkConditionsForm extends Component {
    componentWillMount() {
        this.props.getCurrency.dispatch();
        this.props.getGrossnet.dispatch();
    }

    renderTaxation = (field) => (
        <div className='row card-body m-b-20'>
            <div className='col-sm-9 col-sm-push-3 m-l-20'>
                <TaxationRadioButtons {...field} />
            </div>
        </div>
    );

    render () {
        const { getCurrency: { data: currency }, getGrossnet: { data: grossnet }} = this.props;

        return (
            <div className="card">

                <div className="card-header">
                    <h2>
                        <i className="zmdi zmdi-account-calendar m-r-5"></i>&nbsp;
                        <Translate content='request.headers.workConditions' component='span' />
                    </h2>
                </div>

                <div className='row card-body card-padding'>
                    <div className='col-sm-3 control-label'>
                        <Translate content='request.salary' component='span' />
                    </div>
                    <div className='col-sm-9 p-l-0 p-r-0'>
                        <Field name='salary.amount.from'
                            component={FormGroup} type='number'
                            containerClass='col-sm-4'
                            placeholder={translate('placeholders.from')}
                            validate={[salaryNumberScan, lessThanSalaryTo]} />
                        <Field name='salary.amount.to'
                            component={FormGroup} type='number'
                            containerClass='col-sm-4'
                            placeholder={translate('placeholders.to')}
                            validate={[salaryNumberScan, moreThanSalaryFrom]} />
                        <Field name='salary.currency'
                            component={FormGroup} type='select'
                            list={currency}
                            namePath='code'
                            containerClass='col-sm-4'
                            placeholder={translate('placeholders.currency')} />
                    </div>
                </div>

                <Field name='salary.type'
                    component={this.renderTaxation} type='radio'
                    list={grossnet}
                    namePath='value'
                    label={{ content: 'request.reason' }} />

                <Field name='bonuses'
                    component={FormGroup} type='textarea'
                    label={{ content: 'request.bonuses' }}
                    placeholder={translate('placeholders.enterBonuses')} />

                <Field name='probation'
                    component={FormGroup} type='number'
                    label={{ content: 'request.probation' }}
                    placeholder={translate('placeholders.specifyDays')} />

                <Field name='workplace'
                    component={FormGroup} type='radio'
                    namePath='value'
                    loadName='workplace'
                    label={{ content: 'request.workplace' }} />

                <Field name='schedule' allowClear
                    component={FormGroup} type='select'
                    namePath='value'
                    loadName='schedule'
                    label={{ content: 'request.schedule' }}
                    placeholder={translate('placeholders.chooseSchedule')} />

                <Field name='worktype' allowClear
                    component={FormGroup} type='select'
                    namePath='value'
                    loadName='worktype'
                    label={{ content: 'request.workType' }}
                    placeholder={translate('placeholders.chooseWorkType')} />
            </div>
        );
    }
};
