import React, { Component } from 'react';
import { connect } from 'react-redux';
import Translate from 'react-translate-component';

import ProfileForm from 'rapp/components/views/profile/ProfileForm';

const stateToProps = ({ user: { needUpdatePassword } }) => {
    return { needUpdatePassword };
};

@connect(stateToProps)
export default class Profile extends Component {
    render() {
        const { needUpdatePassword } = this.props;

        return (
            <div className="card row" id="profile-main">
                <div className="col-lg-6 col-md-8 col-sm-10 col-lg-offset-3 col-md-offset-2 col-sm-offset-1 clearfix">
                    <div className="pmb-block p-l-0 p-r-0 p-t-10">
                        <div className="pmbb-body">
                            <div className="pmbb-view">
                                <div className="pmbb-header m-b-20">
                                    <h2 className="c-bluegray">
                                        <i className="zmdi zmdi-info m-r-10" />
                                        <Translate content='profile.privateInfo.title' component="span"/>
                                        <br />
                                        <Translate content='profile.privateInfo.about' component="small"/>
                                    </h2>
                                </div>
                                { needUpdatePassword &&
                                    <Translate className='alert alert-info' content='profile.form.needUpdatePassword' component='div' />
                                }
                                <ProfileForm />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
