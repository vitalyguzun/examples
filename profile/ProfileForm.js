import React, { Component } from 'react';
import { connect } from 'react-redux';
import Translate from 'react-translate-component';
import translate from 'counterpart';
import { reduxForm, Field } from 'redux-form';
import cx from 'classnames';
import { path } from 'ramda';
import { validate } from 'experium-modules';

import Input from 'rapp/components/form/Input';
import { changeUserPassword } from 'rapp/actions/userActions';
import { required, range } from 'rapp/utils/validators';
import { PASSWORD } from 'rapp/constants/regexp';

const passwordRange = range(6,15);
export const passwordLength = (value) => {
    return passwordRange(value) ? translate('errors.passwordLength', {min:6, max: 15}) : undefined;
};

export const passwordSecurityScan = (value) => {
    return !PASSWORD.test(value) ? translate('errors.passwordMustContain', {count: 2}) : undefined;
};

export const matchPasswordNew = (value, model) => {
    return value !== model.passwordRepeat ? translate('errors.passwordsMustMatch') : undefined;
};

export const matchPasswordRepeat = (value, model) => {
    return value !== model.newPassword ? translate('errors.passwordsMustMatch') : undefined;
};

const stateToProps = ({ user, meta: { profile }}) => {
    return { user, meta: profile };
};

const schema = {
    currentPassword: [required],
    newPassword: [required],
    passwordRepeat: [required]
};

@connect(stateToProps, { changeUserPassword })
@reduxForm({ form: 'profileForm', validate: validate(schema) })
export default class ProfileForm extends Component {

    componentWillReceiveProps(props) {
        const { needUpdatePassword } = this.props.user;

        if (needUpdatePassword) {
            this.props.initialize({ currentPassword: 1 });
        }
    }

    onSubmit = ({ currentPassword, newPassword }) => {
        const { changeUserPassword } = this.props;
        changeUserPassword({ currentPassword, newPassword });
    }

    render() {
        const { handleSubmit, dirty, invalid, user: { email, username, login }, meta } = this.props;
        const error = path(['post', 'error', 'response', 'headers', 'x-error-code'], meta);
        const success = path(['post', 'success'], meta);

        return (
            <form className='form-horizontal' onSubmit={this.onSubmit} noValidate>

                { error && <Translate className='alert alert-danger' content={`errors.${error}`} component='div' /> }
                { success && <Translate className='alert alert-success' content='profile.form.passwordChangeSuccess' component='div' /> }

                <div className={cx({ 'has-error': dirty && invalid })}>
                    <dl className='dl-horizontal'>
                        <Translate content='common.email' component='dt'/>
                        <dd>{ email }</dd>
                    </dl>
                    <dl className='dl-horizontal'>
                        <Translate content='common.fullName' component='dt'/>
                        <dd>{ username }</dd>
                    </dl>
                    <dl className='dl-horizontal'>
                        <Translate content='common.login' component='dt'/>
                        <dd>{ login }</dd>
                    </dl>

                    <dl className='dl-horizontal'>
                        <dt className='p-t-10'>
                            <Translate htmlFor='oldPassword' content='profile.form.currentPassword' component='label' />
                        </dt>
                        <dd>
                            <Field name='currentPassword'
                                component={Input}
                                id='oldPassword'
                                type='password'
                                className='form-control'
                                placeholder={translate('placeholders.currentPassword')} />
                        </dd>
                    </dl>

                    <dl className='dl-horizontal'>
                        <dt className='p-t-10'>
                            <Translate htmlFor='newPassword' content='profile.form.newPassword' component='label' />
                        </dt>
                        <dd>
                            <Field name='newPassword'
                                component={Input}
                                id='newPassword'
                                type='password'
                                className='form-control'
                                placeholder={translate('placeholders.newPassword')}
                                validate={[ passwordSecurityScan, passwordLength, matchPasswordNew ]} />
                        </dd>
                    </dl>

                    <dl className='dl-horizontal'>
                        <dt className='p-t-10'>
                            <Translate htmlFor='passwordRepeat' content='profile.form.passwordRepeat' component='label' />
                        </dt>
                        <dd>
                            <Field name='passwordRepeat'
                                component={Input}
                                id='passwordRepeat'
                                type='password'
                                className='form-control'
                                placeholder={translate('placeholders.passwordRepeat')}
                                validate={[ passwordSecurityScan, passwordLength, matchPasswordRepeat ]} />
                        </dd>
                    </dl>

                    <div className='m-b-20'>
                        <Translate component='button' onClick={handleSubmit(this.onSubmit)}
                            className='btn btn-primary btn-sm waves-effect'
                            content='profile.form.updatePassword'
                            disabled={ invalid } />
                    </div>
                </div>
            </form>
        );
    }
}
