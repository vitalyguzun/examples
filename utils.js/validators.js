import not from 'ramda/src/not';
import always from 'ramda/src/always';
import either from 'ramda/src/either';
import gt from 'ramda/src/gt';
import lt from 'ramda/src/lt';
import length from 'ramda/src/length';
import compose from 'ramda/src/compose';

export const required = (error = 'errors.required') => [ not, always({ translate: error }) ];
export const maxValue = (value) => [ lt(value), always({translate: 'errors.valueNotMoreThan', params: { value }}) ];
export const minValue = (value) => [ gt(value), always({translate: 'errors.valueNotLessThan', params: { value }}) ];
export const range = (min, max) => compose(either(gt(min), lt(max)), length);
export const maxLength = (max) => [ compose(lt(max), length), always({translate: 'errors.lengthNotMoreThan', params: { length: max }}) ];
